<?php

class AnonymousController extends Controller {

    public function header() {
	$app = Controller::$app;
	$app->render('header.php',compact('app'));
    }

    public function footer() {
	Controller::$app->render('footer.php');
    }

    public function index(){
	$this->header();
	Controller::$app->render('homepage.php');
	$this->footer();
    }

    public function yopla(){
	$this->header();
	Controller::$app->render('arf.php');
	$this->footer();
    }

    public function affiche_item($id){
	$this->header();
	Controller::$app->render('aff_item.php', compact('id'));
	$this->footer();
    }

    public function insert_info(){
	$app = Controller::$app;
	$nom = $app->request->post('nom');
	$app->flash('info', "J'ai ajouté le nom « $nom »");
	$app->redirectTo('root');
    }
}

?>
